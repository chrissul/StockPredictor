import re
import os

class HelperFunctions:

    #Function that checks a stock code for validity
    #Returns 0 for success and 1 for failure
    @staticmethod
    def checkStockCode(stockName):
        print("checkStockCode")
        if(bool(re.search(r'\d',stockName))):
            return 1
        if(len(stockName) != 4):
            return 1
        return 0

    #Function that checks to see if a model for the stock was created, and if it needs to be updated
    #Returns 0 for model is good, 1 for model needs update, and 2 for model does not exist
    def checkExistingModel(stockName):
        #Check to see if model for the stock exists
        stockName = stockName.upper()
        if (os.path.isfile('models/'+ stockName + '.f')):
            #print("Model for " + stockName + " eixsts!")
            #check for last update date
            if(True):
                return 0
            else:
                return 1
        else:
            return 2
            #print("Not found ... Creating New Predictor Model for " + stockName)
