from src.HelperFunctions import HelperFunctions
import asyncio
from src.Prediction import Prediction

class StockPredictor:

    #Help Information Function
    @staticmethod
    def helpInfo():
        print ("Helper Function Running")
        return 0

    #Input Handler
    def inputHandler(inputValue):
        inputValue = inputValue.lower()
        print("InputValue = " + inputValue)
        #print(inputValue)
        if(inputValue=="predict"):
            stockCode = input("Please enter your stockCode: ")
            #Data Sanitization

            @asyncio.coroutine
            def main():
                #Start the function trail

                #Step One: Stock Validation
                result = (HelperFunctions.checkStockCode(stockCode))
                if(result==1):
                    print("Stock Code Validation: Invalid Stock Code")
                    return 1
                else:
                    print("Stock Code Validation: Success")

                #Step Two: Check for existing Model
                result = (HelperFunctions.checkExistingModel(stockCode))
                if(result==1):
                    print("Check for Existing Model: Needs Update")
                    Prediction.updateModel(stockCode)
                elif(result==2):
                    print("Check for Existing Model: Does Not Exist")
                    Prediction.makeNewModel(stockCode)
                else:
                    print("Check for Existing Model: Success")

                #Step Three: Prediction Output
                print("Prediction Output")

                return 0


            loop = asyncio.get_event_loop()
            loop.run_until_complete(main())

        elif(inputValue=="help"):
            print("Stock Predictor Version 1.0")
            print("Current available functions:")
            print("(predict, help, quit)")
            return 0
        elif (inputValue=="quit"):
            quit()
        else:
            return 1
